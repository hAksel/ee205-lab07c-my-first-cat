///////////////////////////////////////////////////////////////
//          EE 205 OBJECT ORIENTED PROGRAMMING
//          LAB 7C MY FIRST CAT 
// purpose: get used to objects.
//
// usage: this program will print hello world using three different methods
//
// compilation: make, or make test (compile and run). remember to <make clean> 
//                to remove old object files 
//
// author: Aksel Sloan  <aksel@hawaii.edu>
// date: 7 march 2022 
///////////////////////////////////////////////////////////////
#include <iostream>

using namespace std; 

//like enums, use Capital on the class definition
class Cat {
public:
   void sayHello() {
      // Add the code to write “Meow" to the console
      cout << "meow world" << endl; //to terminal 
      
   }
};

int main() {
   //instatiate the class
   Cat myCat;
   myCat.sayHello();
   return 0; 
}
