///////////////////////////////////////////////////////////////
//          EE 205 OBJECT ORIENTED PROGRAMMING
//          LAB 7C MY FIRST CAT 
// purpose: get used to objects.
//
// usage: this program will print hello world using three different methods
//
// compilation: make, or make test (compile and run). remember to <make clean> 
//                to remove old object files 
//
// author: Aksel Sloan  <aksel@hawaii.edu>
// date: 7 march 2022 
///////////////////////////////////////////////////////////////
#include <iostream>

int main() {
   std::cout << "heLLLOO world!" << std::endl;
   return 0;
}

